from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from campaigns.views import  CampaignListView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', CampaignListView.as_view(), name='home'),
    url(r'^login/$', 'accounts.views.custom_login',
        {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',
        {'template_name': 'logout.html', 'next_page': '/'}, name='logout'),
    url(r'^social_auth/', include('social_auth.urls')),
    url(r'^account/', include('accounts.urls')),
    url(r'^campaign/', include('campaigns.urls')),
    url(r'^ads/', include('ads.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
