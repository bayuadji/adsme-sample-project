from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator

from .forms import CampaignForm
from .models import Campaign


class CampaignListView(ListView):
    model = Campaign
    template_name = 'campaigns/list.html'
    context_object_name = 'objects'
    paginate_by = 10

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampaignListView, self).dispatch(*args, **kwargs)


class CampaignDetailView(DetailView):
    model = Campaign

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampaignDetailView, self).dispatch(*args, **kwargs)


class CampaignCreateView(CreateView):
    model = Campaign
    form_class = CampaignForm
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampaignCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CampaignCreateView, self).form_valid(form)


class CampaignDeleteView(DeleteView):
    model = Campaign
    success_url = '/'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampaignDeleteView, self).dispatch(*args, **kwargs)


class CampaignUpdateView(UpdateView):
    model = Campaign

    form_class = CampaignForm

    def get_context_data(self, **kwargs):
	context = super(CampaignUpdateView, self).get_context_data(**kwargs)
	context['title'] = 'Update'
	return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CampaignUpdateView, self).dispatch(*args, **kwargs)


