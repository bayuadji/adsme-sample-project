from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import CampaignDetailView, CampaignCreateView, CampaignDeleteView, CampaignUpdateView

urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)/$', CampaignDetailView.as_view(), name='campaign.detail'),
    url(r'^(?P<pk>\d+)/edit/$', CampaignUpdateView.as_view(), name='campaign.update'),
    url(r'^(?P<pk>\d+)/delete/$', CampaignDeleteView.as_view(), name='campaign.delete'),
    url(r'add/$', CampaignCreateView.as_view(), name='campaign.create'),
)
