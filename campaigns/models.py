from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models

CAMPAIGN_STATUS = [
    (0, 'STOP'),
    (1, 'ACTIVE'),
    (2, 'PAUSE')
    ]


class Campaign(models.Model):
    """
    Campaign model.
    currently hold the main data.
    furthermore we can make based on google-campagin or facebook campaign.
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=256)
    created_ts = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=CAMPAIGN_STATUS,
                                 default=1)
    daily_budget = models.IntegerField()
    lifetime_budget = models.IntegerField()
    start_ts = models.DateTimeField()
    end_ts = models.DateTimeField()
    is_google = models.BooleanField(default=False)
    is_facebook = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('campaign.detail', kwargs={'pk': self.id})
