from django.contrib import admin

from .models import Campaign


class CampaignAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_ts', 'user')


admin.site.register(Campaign, CampaignAdmin)
