from django.db import models

from campaigns.models import Campaign
from ads.models import Ads

class FacebookCampaign(models.Model):
    campaign = models.ForeignKey(Campaign)
    campaign_facebook_id = models.CharField(max_length=128, null=True, blank=True)
    last_update_ts = models.DateTimeField(auto_now=True)
    raw_data_result = models.TextField(null=True, blank=True)


class FacebookAds(models.Model):
    ads = models.ForeignKey(Ads)
    ads_facebook_id = models.CharField(max_length=128, null=True, blank=True)
    last_update_ts = models.DateTimeField(auto_now=True)
    raw_data_result = models.TextField(null=True, blank=True)
