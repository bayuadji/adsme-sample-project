from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.contrib.auth.models import User


from campaigns.models import Campaign
from ads.models import Ads

import tasks


@receiver(post_save, sender=Campaign)
def create_campaign(sender, instance, created, **kwargs):
    try:
        if created:
            tasks.create_campaigns.delay(instance.id)
        else:
            tasks.update_campaigns.delay(instance.id)
    except Exception, e:
        pass


@receiver(post_save, sender=Ads)
def create_ads(sender, instance, created, **kwargs):
    if created:
        tasks.create_ads.delay(instance.id)
    else:
        tasks.update_ads.delay(instance.id)


@receiver(pre_delete, sender=Campaign)
def delete_campaigns(sender, instance, signal, **kwargs):
    tasks.delete_campaigns.delay(instance.id)


@receiver(pre_delete, sender=Ads)
def delete_ads(sender, instance, **kwargs):
    tasks.delete_ads.delay(instance.id)
