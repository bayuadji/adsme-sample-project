import requests as r
from celery import task

from django.contrib.auth.models import User

from ads.models import Ads
from campaigns.models import Campaign
from accounts import utils as accounts_utils

from .models import FacebookCampaign

FACEBOOK_API_URL = 'https://graph.facebook.com/'


@task
def create_campaigns(campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    user = campaign.user
    facebook_campaign, is_created = FacebookCampaign.objects.get_or_create(campaign=campaign)
    try:
        parameter = {
        'name': campaign.name,
        'start_time': campaign.start_ts.isoformat,
        'campaign_status': 1,
        'access_token': account_utils.get_token(user, facebook)
        }
        if campaign.end_ts:
            paramater['daily_budget'] = campaign.daily_budget
            parameter['end_time'] = campaign.end_ts.isoformat
        else:
            parameter['lifetime_budget'] = campaign.lifetime_budget
        result = r.post('%s/act_%s/adcampaigns' % (FACEBOOK_API_URL,
            account_utils.get_id(campaign.user, 'facebook')),
            parameter)
        if 'id' in result:
            facebook_campaign.facebook_campaign_id = result['id']
            facebook_campaign.raw_data_result = result.content
    except Exception, e:
        facebook_campaign.raw_data_result = str(e)
        result = str(e)
    facebook_campaign.save()
    return result


@task
def update_campaigns(campaign_id, username):
    campaign = Campaign.objects.get(id=campaign)
    parameter = {
        'name': campaign.name,
        'start_time': campaign.start_ts.isoformat,
        'campaign_status': campaign.status,
        'access_token': user.social_auth.filter(provider='facebook')[0].access_token['access_token']
        }
    if campaign.end_ts:
        paramater['daily_budget'] = campaign.daily_budget
        parameter['end_time'] = campaign.end_ts.isoformat
    else:
        parameter['lifetime_budget'] = campaign.lifetime_budget
    fcb_campaign = FacebookCampaign.objects.get(campaign=campaign)
    result = r.post('%s/%s' % (FACEBOOK_API_URL,
                               fcb_campaign.facebook_campaign_id), parameter)


@task
def delete_campaigns(campaign_id, username):
    fcb_campaign = FacebookCampaign.objects.get(campaign__id=campaign_id)
    result = r.delete('%s/%s' % (FACEBOOK_API_URL, fcb_campaign.facebook_campaign_id))

    return result


@task
def create_ads(ads_id):
    user = User.objects.get(username=username)
    ads = Ads.objects.get(id=ads_id)
    parameter = {
        'name': ads.name,
        'type': '1',
        'title': ads.title,
        'body': ads.body,
        'link_url': ads.url,
        'access_token': user.social_auth.filter(provider='facebook')[0].access_token['access_token']
        }

    result = r.post('%s/act_%s/adcreatives' % (FACEBOOK_API_URL,
                                              user.social_auth.filter(provider='facebook')[0].uid),
                                              parameter)
    result_json = json.loads(result)
    parameter = {
        'name': ads.name,
        'campaign': ads.campaign.campaign_id,
        'bid_type': 'CPC',
        'creative': {'creative_id': result_json['creative_id']},
        }
    result = r.post('%s/act_%s/adgroups' % (FACEBOOK_API_URL,
                    user.social_auth.filter(provider='facebook')[0].uid),
                    parameter)
    return result


@task
def update_ads(ads):
    #still not clear what to do when update ads.
    pass


@task
def delete_ads(ads_id):
    ads = Ads.objects.get(id=ads_id)
    result = r.delete('%s/%s' % (FACEBOOK_API_URL,
                                 ads.facebook_ads.group_id))
    return result
