from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator

from .models import Ads


class AdsCreateView(CreateView):
    model = Ads
    success_url = reverse_lazy('home')
        
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AdsCreateView, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        from campaigns.models import Campaign

        map_ = super(AdsCreateView, self).get_form_kwargs()
        if 'campaign_id' in self.kwargs:
            map_['initial']['campaign'] = Campaign.objects.get(id=int(self.kwargs.get('campaign_id')))
        return map_

    def get_form(self, form_class):
        return form_class(**self.get_form_kwargs())


class AdsUpdateView(UpdateView):
    model = Ads
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super(AdsUpdateView, self).get_context_data(**kwargs)
        context['btn_title'] = 'Update'
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AdsUpdateView, self).dispatch(*args, **kwargs)


class AdsDeleteView(DeleteView):
    model = Ads
    success_url = reverse_lazy('home')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AdsDeleteView, self).dispatch(*args, **kwargs)
