from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import AdsCreateView, AdsUpdateView, AdsDeleteView

urlpatterns = patterns('',
                       #url(r'^(?P<pk>\d+)/$', CampaignDetailView.as_view(), name='campaign.detail'),
    url(r'^(?P<pk>\d+)/edit/$', AdsUpdateView.as_view(), name='ads.update'),
    url(r'^(?P<pk>\d+)/delete/$', AdsDeleteView.as_view(), name='ads.delete'),
    url(r'^(?P<campaign_id>\d+)/add/$', AdsCreateView.as_view(), name='ads.create_campaign'),
    url(r'add/$', AdsCreateView.as_view(success_url='/'), name='ads.create'),
)
