from django.db import models

from campaigns.models import Campaign


class Ads(models.Model):
    campaign = models.ForeignKey(Campaign)
    title = models.CharField(max_length=256)
    body = models.CharField(max_length=256)
    image = models.ImageField(upload_to='ads_images')
    url = models.URLField()
    is_google = models.BooleanField(default=False)
    is_facebook = models.BooleanField(default=False)
