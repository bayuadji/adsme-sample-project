"""Worker script to communicate with the AdWords API Python client library."""


import sys
import time


from adspygoogle.adwords.AdWordsClient import AdWordsClient
from adspygoogle.adwords.AdWordsErrors import AdWordsError
from adspygoogle.common import PYXML
from adspygoogle.common import Utils


# Enable to see debugging data in App Engine's console, when API call is made.
DEBUG = False
SERVER = 'https://adwords.google.com'
VERSION = 'v201306'

DEMO_VERSION = '4.3.0'


def SetUpClient(email, auth_token, developer_token):
  """Return instance of Client.

  Args:
    email: str Login email for the Google Account.
    password: str Password for the Google Account.
    developer_token: str Developer token for AdWords API.

  Returns:
    Client instance.
  """
  client = AdWordsClient(
      path='.',
      headers={
          'email': email,
          'authToken': auth_token,
          'userAgent': 'AppEngine Demo v%s' % DEMO_VERSION,
          'developerToken': developer_token},
      # The app engine doesn't like when data is written to disc. Thus, we need
      # to explicitly turn off logging. Though, if debugging is enabled, the
      # data from STDOUT will be redirected to app engine's debugging console.
      config={
          'debug': Utils.BoolTypeConvert(DEBUG),
          'xml_log': 'n',
          'request_log': 'n',
          'pretty_xml': 'n',
          'xml_parser': PYXML,
          'compress': 'n'
      })
  client.use_mcc = True
  return client


def GetAccountInfo(client):
  """Return HTML formatted results of the GetAccountInfo() call.

  Args:
    client: Instance of AdWordsClient.

  Returns:
    list List of strings with output data to send to the UI.
  """
  client.use_mcc = True
  mcs_service = client.GetManagedCustomerService(version=VERSION)
  selector = {
      'fields': ['Login', 'CustomerId', 'Name', 'TestAccount'],
      'predicates': [{
          'field': 'CanManageClients',
          'operator': 'EQUALS',
          'values': ['false']}]
  }
  accounts = mcs_service.Get(selector)[0]['entries']
  client.use_mcc = False

  return accounts


def GetCampaignInfo(client):
  """Return HTML formatted results of all campaigns.

  Args:
    client: Instance of AdWordsClient.

  Returns:
    list List of strings with output data to send to the UI.
  """
  client.use_mcc = False
  # A somewhat hackish workaround for "The read operation timed out" error,
  # which could be triggered on AppEngine's end if the request is too large and
  # is taking too long.
  max_tries = 3
  total_campaigns = 0
  today = time.strftime('%Y%m%d', time.localtime())
  for i in xrange(1, max_tries + 1):
    try:
      selector = {
          'fields': ['Id', 'Name', 'Status', 'AveragePosition', 'Clicks',
                     'Impressions', 'Amount'],
          'dateRange': {
              'min': today,
              'max': today
          }
      }
      campaigns = client.GetCampaignService(SERVER, VERSION).Get(selector)[0]
      total_campaigns = int(campaigns['totalNumEntries'])
      if total_campaigns > 0:
        campaigns = campaigns['entries']
      break
    except Exception, e:
      if i == max_tries:
        raise AdWordsError(e)
      continue

    return campaigns
  
def CreateCampaign(client, name, budget=1):
  """Create a new campaign for a given budget amount.

  Args:
    client: Instance of AdWordsClient.
    budget: int a budget amount to use.
  """
  budget_service = client.GetBudgetService(SERVER, VERSION)
  budget = {
      'name': 'Interplanetary budget #%s' % time.time(),
      'amount': {
          'microAmount': str(int(float(budget) * 1000000))
      },
      'deliveryMethod': 'STANDARD',
      'period': 'DAILY'
  }
  budget_operations = [{
      'operator': 'ADD',
      'operand': budget
  }]
  budget_id = budget_service.Mutate(budget_operations)[0]['value'][0][
      'budgetId']

  campaign_service = client.GetCampaignService(SERVER, VERSION)
  operations = [{
      'operator': 'ADD',
      'operand': {
          'name': 'Campaign #%s%s' % (name, time.time()),
          'status': 'PAUSED',
          'biddingStrategyConfiguration': {
              'biddingStrategyType': 'MANUAL_CPC'
          },
          'budget': {
              'budgetId': budget_id
          },
          'settings': [
              {
                  'xsi_type': 'KeywordMatchSetting',
                  'optIn': 'false'
              }
          ]
      }
  }]
  return campaign_service.Mutate(operations)


def GetAdGroupInfo(client, campaign_id):
  """Return HTML formatted results of all existing ad groups, for a given
  campaign id.

  Args:
    client: Client an instance of Client.
    campaign_id: str id of the campaign for which to fetch ad groups.

  Returns:
    list a list of strings with output data to send to the UI.
  """
  client.use_mcc = False
  selector = {
      'fields': ['Id', 'Name', 'KeywordMaxCpc', 'Status'],
      'predicates': [
          {
              'field': 'CampaignId',
              'operator': 'EQUALS',
              'values': [campaign_id]
          }
      ]
  }
  ad_groups = client.GetAdGroupService(SERVER, VERSION).Get(selector)[0]
  total_ad_groups = int(ad_groups['totalNumEntries'])
  if total_ad_groups > 0:
    ad_groups = ad_groups['entries']

  return ad_groups


def CreateAdGroup(client, campaign_id, max_cpc=1):
  """Create a new ad group.

  Args:
    client: Instance of AdWordsClient.
    campaign_id: str Id of the campaign to use.
    max_cpc: int Keyword max cpc to use.
  """
  ad_group_service = client.GetAdGroupService(SERVER, VERSION)
  operations = [{
      'operator': 'ADD',
      'operand': {
          'campaignId': campaign_id,
          'name': 'AdGroup #%s' % time.time(),
          'status': 'ENABLED',
          'biddingStrategyConfiguration': {
              'bids': [
                  {
                      'xsi_type': 'CpcBid',
                      'bid': {
                          'microAmount': str(int(float(max_cpc) * 1000000))
                      },
                  }
              ]
          }
      }
  }]
  ad_group_service.Mutate(operations)
