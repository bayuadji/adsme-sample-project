"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import datetime

from mock import patch

from django.test import TestCase
from django.contrib.auth.models import User

from campaigns.models import Campaign

from . import tasks


class GoogleAdsApi(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='djibon',
                                        password='password',
                                        email='email')
        self.campaign = Campaign.objects.create(user=self.user,
                                                name='name',
                                                status=1,
                                                daily_budget=100,
                                                lifetime_budget=100,
                                                start_ts=datetime.datetime.now(),
                                                end_ts=datetime.datetime.now() + datetime.timedelta(days=1),
                                                is_facebook=True,
                                                is_google=True)

    @patch('accounts.utils.get_token')
    def test_add_api(self, account_patch):
        account_patch.return_value = "TESTTOKEN"
        tasks.create_campaign(self.campaign.id)
