#!/usr/bin/python
#
# Copyright 2010 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Client script to handle user interface.

Responsible for building web forms, providing get and post handlers, and
maintaining user interface.
"""

import logging
import os

import wsgiref.handlers
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

try:
  import api_worker
except ImportError, e:
  if e.message.find('pyexpat') > -1:
    raise Exception(
        'Incompatible version of pyexpat. Please see step 7 in README.')
  raise Exception(e)


# Enable to see a stack trace in a browser, when a handler raises an exception.
DEBUG = False

LOGIN = 'INSERT_GOOGLE_ACCOUNT_LOGIN_EMAIL_HERE'
PASSWORD = 'INSERT_GOOGLE_ACCOUNT_PASSWORD_HERE'
DEVELOPER_TOKEN = 'INSERT_DEVELOPER_TOKEN_HERE'


class AppUser(db.Model):

  """Implements AppUser.

  The AppUser represents an API user in a datastore. For each user, we store
  user's identifier (email address), the date on which API requests were made,
  the login email of the Google/AdWords account, and the number of operations
  consumed during all transaction on a given day.
  """

  user = db.UserProperty()
  date = db.DateProperty(auto_now=True)
  login = db.EmailProperty()
  operations = db.IntegerProperty()


class MainPage(webapp.RequestHandler):

  """Implements MainPage."""

  def get(self):
    """Handle get request."""
    output = []
    client = None
    try:
      try:
        output.append('Using login: %s<br/><br/>' % LOGIN)

        # Load Client instance.
        client = api_worker.SetUpClient(LOGIN, PASSWORD, DEVELOPER_TOKEN)

        # Fetch account info for each client account.
        output.extend(api_worker.GetAccountInfo(client))
      except Exception, e:
        msg = str(e)
        # Example for handling API errors, error code 9 in this case.
        if 'code' in e.__dict__ and e.code == 9:
          msg = ('%s Make sure to modify \'adwordsapi_demo/api_client.py\' to '
                 'use your <a href="https://www.google.com/accounts/NewAccount'
                 '">Google Account</a>\'s login and password?' % msg)
        output.append('&nbsp;<font color="red"><b>Error</b></font>: %s' % msg)
    finally:
      # Update current user's API usage in the datastore.
      if client is not None:
        UpdateApiUsage(LOGIN, client.GetOperations())

      # Use template to write output to the page.
      template_values = {'output': output}
      path = os.path.join(os.path.dirname(__file__), 'index.html')
      self.response.out.write(unicode(template.render(path, template_values)))


class ShowCampaigns(webapp.RequestHandler):

  """Implements ShowCampaigns."""

  def get(self):
    """Handle get request."""
    client_customer_id = self.request.get('clientCustomerId')

    output = []
    try:
      try:
        # Load Client instance.
        client = api_worker.SetUpClient(LOGIN, PASSWORD, DEVELOPER_TOKEN)
        client.SetClientCustomerId(client_customer_id)

        output.append('&nbsp;<b>Customer ID</b>: %s<br/><br/><br/>' %
                      client_customer_id)

        # Fetch campaigns from a given client account. All URL parameters are
        # base64 encoded, so that data is not in clear text (optional).
        output.append(''.join(api_worker.GetCampaignInfo(client)).replace(
            'clientCustomerId=None', 'clientCustomerId=%s'
            % client_customer_id))
        output.append(("""
            <br/><br/><br/>
            <form action="/addCampaign?clientCustomerId=%s" method="post"
                  id="addCampaignForm" style="display: none;">
              <table>
                <tr>
                  <th><label for="budget">Budget ($): </label></th>
                  <td><input type="text" name="budget" size="35"/></td>
                </tr>
                <tr align="right">
                  <td colspan="2">
                    <input type="submit" value="Add Campaign"/>
                  </td>
                </tr>
              </table>
            </form>
            <div onClick="document.getElementById('addCampaignForm').style.
                 display='block'; this.style.display='none';"
                 style="cursor:pointer;">
                 &nbsp;[+] Expand to add campaign</div>"""
            % client_customer_id))
        output.append(('<br/><br/>&nbsp;<a href="/">Back to accounts page</a>'))
      except Exception, e:
        output.append('&nbsp;<font color="red"><b>Error</b></font>: %s' % e)
        output.append(('<br/><br/><br/><a href="/">Back to accounts</a>'))
    finally:
      # Update current user's API usage in the datastore.
      UpdateApiUsage(LOGIN, client.GetOperations())

      # Use template to write output to the page.
      template_values = {'output': output}
      path = os.path.join(os.path.dirname(__file__), 'index.html')
      self.response.out.write(unicode(template.render(path, template_values)))

  def post(self):
    """Handle post request."""
    client_customer_id = self.request.get('clientCustomerId')
    if not client_customer_id:
      self.redirect('/')
    else:
      self.redirect('/showCampaigns?clientCustomerId=%s' % client_customer_id)


class AddCampaign(webapp.RequestHandler):

  """Implements AddCampaign."""

  def post(self):
    """Handle post request."""
    client_customer_id = self.request.get('clientCustomerId')

    output = []
    try:
      try:
        # Load Client instance.
        client = api_worker.SetUpClient(LOGIN, PASSWORD, DEVELOPER_TOKEN)
        client.use_mcc = False
        client.SetClientCustomerId(client_customer_id)

        # Create new campaign.
        api_worker.CreateCampaign(client, self.request.get('budget'))

        self.redirect('/showCampaigns?clientCustomerId=%s'
                      % client_customer_id)
      except Exception, e:
        output.append('&nbsp;<font color="red"><b>Error</b></font>: %s' % e)
        output.append(('<br/><br/><br/><a href="/showCampaigns?'
                       'clientCustomerId=%s">Back to campaigns</a>'
                       % client_customer_id))

        # Use template to write output to the page.
        template_values = {'output': output}
        path = os.path.join(os.path.dirname(__file__), 'index.html')
        self.response.out.write(unicode(template.render(path, template_values)))
    finally:
      # Update current user's API usage in the datastore.
      UpdateApiUsage(LOGIN, client.GetOperations())


class ShowAdGroups(webapp.RequestHandler):

  """Implements ShowAdGroups."""

  def get(self):
    """Handle get request."""
    client_customer_id = self.request.get('clientCustomerId')
    campaign_id = self.request.get('campaignId')

    output = []
    try:
      try:
        # Load Client instance.
        client = api_worker.SetUpClient(LOGIN, PASSWORD, DEVELOPER_TOKEN)
        client.SetClientCustomerId(client_customer_id)

        output.append('&nbsp;<b>Customer ID</b>: %s<br/>' % client_customer_id)
        output.append('&nbsp;<b>CampaignId</b>: %s<br/><br/><br/>'
                      % campaign_id)

        # Fetch ad groups from a given campaign.
        output.append(''.join(api_worker.GetAdGroupInfo(client, campaign_id)))

        output.append("""
            <br/><br/><br/>
            <form action="/addAdGroup?clientCustomerId=%s&campaignId=%s"
                  method="post" id="addAdGroupForm" style="display: none;">
              <table>
                <tr>
                  <th><label for="maxCpc">MaxCpc ($): </label></th>
                  <td><input type="text" name="maxCpc" size="35"/></td>
                </tr>
                <tr align="right">
                  <td colspan="2">
                    <input type="submit" value="Add AdGroup"/>
                  </td>
                </tr>
              </table>
            </form>
            <div onClick="document.getElementById('addAdGroupForm').style.
                 display='block'; this.style.display='none';"
                 style="cursor:pointer;">
                 &nbsp;[+] Expand to add ad group</div>"""
            % (client_customer_id, campaign_id))
        output.append('<br/><br/>&nbsp;<a href="/showCampaigns?'
                      'clientCustomerId=%s">Back to campaigns page</a>'
                      % client_customer_id)
      except Exception, e:
        output.append('&nbsp;<font color="red"><b>Error</b></font>: %s' % e)
        output.append('<br/><br/><br/><a href="/showCampaigns?'
                      'clientCustomerId=%s">Back to campaigns</a>'
                      % client_customer_id)
    finally:
      # Update current user's API usage in the datastore.
      UpdateApiUsage(LOGIN, client.GetOperations())

      # Use template to write output to the page.
      template_values = {'output': output}
      path = os.path.join(os.path.dirname(__file__), 'index.html')
      self.response.out.write(unicode(template.render(path, template_values)))

  def post(self):
    """Handle post request."""
    client_customer_id = self.request.get('clientCustomerId')
    campaign_id = self.request.get('campaignId')
    if not client_customer_id or not campaign_id:
      self.redirect('/')
    else:
      self.redirect('/showAdGroups?clientCustomerId=%s&campaignId=%s'
                    % (client_customer_id, campaign_id))


class AddAdGroup(webapp.RequestHandler):

  """Implements AddAdGroup."""

  def post(self):
    """Handle post request."""
    client_customer_id = self.request.get('clientCustomerId')
    campaign_id = self.request.get('campaignId')

    output = []
    try:
      try:
        # Load Client instance.
        client = api_worker.SetUpClient(LOGIN, PASSWORD, DEVELOPER_TOKEN)
        client.use_mcc = False
        client.SetClientCustomerId(client_customer_id)

        # Create new ad group.
        api_worker.CreateAdGroup(client, campaign_id,
                                 self.request.get('maxCpc'))

        self.redirect('/showAdGroups?clientCustomerId=%s&campaignId=%s'
                      % (client_customer_id, campaign_id))
      except Exception, e:
        output.append('&nbsp;<font color="red"><b>Error</b></font>: %s' % e)
        output.append('<br/><br/><br/><a href="/showAdGroups?'
                      'clientCustomerId=%s&campaignId=%s">Back to ad groups</a>'
                      % (client_customer_id, campaign_id))
    finally:
      # Update current user's API usage in the datastore.
      UpdateApiUsage(LOGIN, client.GetOperations())

      # Use template to write output to the page.
      template_values = {'output': output}
      path = os.path.join(os.path.dirname(__file__), 'index.html')
      self.response.out.write(unicode(template.render(path, template_values)))


def InitUser():
  """Initialize application user.

  Retrieve existing user from a datastore or add new user.

  Returns:
    AppUser instance of the application user.
  """
  # Retrieve existing user from a datastore or add a new user.
  app_users = db.GqlQuery('SELECT * FROM AppUser WHERE '
                          'user = :user AND login = :login',
                          user=users.get_current_user(), login=LOGIN)
  app_user = None
  for a_user in app_users:
    app_user = a_user
  if not app_user:
    app_user = AppUser(user=users.get_current_user(), login=LOGIN, operations=0)
    db.put(app_user)

  return app_user


def UpdateApiUsage(login, operations):
  """Update current user's API usage in the datastore.

  Args:
    login: str the login email in use.
    operations: int the number of operations consumed during API transaction.
  """
  app_user = InitUser()
  app_user.operations += operations
  app_user.put()


def main():
  if DEBUG:
    logging.getLogger().setLevel(logging.DEBUG)
  application = webapp.WSGIApplication([('/', MainPage),
                                        ('/showCampaigns', ShowCampaigns),
                                        ('/addCampaign', AddCampaign),
                                        ('/showAdGroups', ShowAdGroups),
                                        ('/addAdGroup', AddAdGroup)],
                                       debug=DEBUG)
  wsgiref.handlers.CGIHandler().run(application)


if __name__ == '__main__':
  main()
