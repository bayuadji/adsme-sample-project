from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.contrib.auth.models import User


from campaigns.models import Campaign
from ads.models import Ads

from . import tasks


@receiver(post_save, sender=Campaign)
def create_campaign(sender, instance, created, **kwargs):
    if created:
        tasks.create_campaign.delay(instance.id)
    else:
        tasks.update_campaign.delay(instance.id)


@receiver(post_save, sender=Ads)
def create_ads(sender, instance, created, **kwargs):
    if created:
        tasks.create_ads.delay(instance.id)
    else:
        tasks.update_ads.delay(instance.id)


@receiver(pre_delete, sender=Campaign)
def delete_campaigns(sender, instance, signal, **kwargs):
    tasks.delete_campaign.delay(instance.id)


@receiver(pre_delete, sender=Ads)
def delete_ads(sender, instance, **kwargs):
    tasks.delete_ads.delay(instance.id)
