from celery import task

from django.contrib.auth.models import User

from django.conf import settings

from campaigns.models import Campaign
from accounts import utils as account_utils

from .models import GoogleCampaign

try:
    import api_worker
except ImportError, e:
    raise Exception(e)

DEVELOPER_TOKEN = getattr(settings, 'FACEBOOK_ADS_DEVELOPER_TOKEN', 'TOKEN')


@task
def create_campaign(campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    #get the account id first
    google_campaign, is_created = GoogleCampaign.objects.get_or_create(campaign=campaign)
    value_campaign = None
    try:
        client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                        account_utils.get_token(campaign.user, 'google-oauth2'),
                                        DEVELOPER_TOKEN)
        account = api_worker.GetAccountInfo(client)

        #create the campaigns
        client.use_mcc = False
        client.SetClientCustomerId(account['CustomerId'])

        value_campaign = api_worker.CreateCampaign(client, campaign.lifetime_budget,
                                               campaign.name)
        if 'id' in result:
            google_campaign.google_campaign_id = value_campaign['id']
            google_campaign.raw_data_result = str(value_campaign)
        google_campaign.save()
        add_group(campaign_id)

    except Exception, e:
        google_campaign.raw_data_result = str(e)
        value_campaign = str(e)
        google_campaign.save()
    return value_campaign


@task
def update_campaign(campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
        account_utils.get_token(campaign.user, 'google-oauth2'),
        DEVELOPER_TOKEN)
    account = api_worker.GetAccountInfo(client)

    #create the campaigns
    client.use_mcc = False
    client.SetClientCustomerId(account['CustomerId'])

    campaign_service = client.GetCampaignService(SERVER, VERSION)
    operands = {
            'id': campaign.google_campaign_id,
            'status': campaign.status,
            'startDate': campaign.start_ts.strftime('Ymd')
        }
    if campaign.end_ts:
        operands['endDate'] = campaign.end_ts.strftime('Ymd')

    operations = [{
        'operator': 'SET',
        'operands': operands
    }]

    return campaign_service.mutate(operations)


@task
def delete_campaign(campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                    account_utils.get_token(campaign.user, 'google-oauth2'),
                                    DEVELOPER_TOKEN)
    account = api_worker.GetAccountInfo(client)

    #create the campaigns
    client.use_mcc = False
    client.SetClientCustomerId(account['CustomerId'])

    campaign_service = client.GetCampaignService(SERVER, VERSION)
    operations = [{
        'operator': 'SET',
        'operand': {
            'id': campaign.google_campaign_id,
            'status': 'DELETED'
            }}]

    return campaign_service.MUTATE(operations)[0]


@task
def add_group(campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                    account_utils.get_token(campaign.user, 'google-oauth2'),
                                    DEVELOPER_TOKEN)
    account = api_worker.GetAccountInfo(client)

    #create the campaigns
    client.use_mcc = False
    client.SetClientCustomerId(account['CustomerId'])

    ad_group_service = client.GetAdGroupService(SERVER, VERSION)
    # Construct operations and add ad groups.
    operations = [{
        'operator': 'ADD',
        'operand': {
            'campaignId': campaign_id,
            'name': 'Earth to Mars Cruises #%s' % Utils.GetUniqueName(),
            'status': 'ENABLED',
            'bids': {
                'xsi_type': 'ManualCPCAdGroupBids',
                'keywordMaxCpc': {
                    'amount': {
                        'microAmount': '1000000'
                        }
                        },
                        # Optional field.
                        'keywordContentMaxCpc': {
                            'amount': {
                                'microAmount': '2000000'
                        }
                }
        }
    }
    }, {
        'operator': 'ADD',
        'operand': {
            'campaignId': campaign_id,
            'name': 'Earth to Venus Cruises #%s' % Utils.GetUniqueName(),
            'status': 'ENABLED',
            'bids': {
                'xsi_type': 'ManualCPCAdGroupBids',
                'keywordMaxCpc': {
                    'amount': {
                        'microAmount': '2000000'
                    }
                },
            }
        }
    }]
    ad_groups = ad_group_service.Mutate(operations)[0]
    campaign.group_id = ads_groups[0]['id']
    campaign.save()
    return ads_groups


@task
def create_ads(ads_id):
    ads = Ads.objects.get(id=ads_id)
    try:
        client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                        account_utils.get_token(ads.campaign.user, 'google-oauth2'),
                                        DEVELOPER_TOKEN)
        account = api_worker.GetAccountInfo(client)

        #create the campaigns
        client.use_mcc = False
        client.SetClientCustomerId(account['CustomerId'])

        ad_group_ad_service = client.GetAdGroupAdService(SERVER, VERSION)
        operations = [
            {
                'operator': 'ADD',
                'operand': {
                'xsi_type': 'AdGroupAd',
                'adGroupId': ads.campaign.google_campaign.group_id,
                'url': ads.url,
                'displayUrl': ads.url,
                'description': ads.body,
                'headline': ads.headline}
                }]
        result = ad_group_service.Mutate(operations)[0]
        ads.ads_google_id = result[0]['ad']['id']
    except Exception, e:
        result = str(e)

    ads.raw_data_result = str(result)
    ads.save()
    return result


@task
def edit_ads(ads):
    ads = Ads.objects.get(id=ads_id)
    client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                    account_utils.get_token(ads.campaign.user, 'google-oauth2'),
                                    DEVELOPER_TOKEN)
    account = api_worker.GetAccountInfo(client)

    #create the campaigns
    client.use_mcc = False
    client.SetClientCustomerId(account['CustomerId'])

    ad_group_ad_service = client.GetAdGroupAdService(SERVER, VERSION)
    operations = [
        {
            'operator': 'SET',
            'operand': {
                'xsi_type': 'AdGroupAd',
                'adGroupId': ads.campaign.google_campaign.group_id,
                'url': ads.url,
                'displayUrl': ads.url,
                'description': ads.body,
                'headline': ads.headline}
        }]
    result = ad_group_service.Mutate(operations)[0]
    return result


@task
def delete_ads(ads):
    ads = Ads.objects.get(id=ads_id)
    client = api_worker.SetUpClient(settings.GOOGLE_ADS_API_EMAIL,
                                    account_utils.get_token(ads.campaign.user, 'google-oauth2'),
                                    DEVELOPER_TOKEN)
    account = api_worker.GetAccountInfo(client)

    #create the campaigns
    client.use_mcc = False
    client.SetClientCustomerId(account['CustomerId'])

    ad_group_ad_service = client.GetAdGroupAdService(SERVER, VERSION)

    operations = [{
        'operator': 'REMOVE',
        'operand': {
            'xsi_type': 'AdGroupdAd',
            'adGroupId': ads.campaign.google_campaign.group_id,
            'ad': {
                'id': None,
                }
            }
        }]
    return ad_group_ad_service.Mutate(operations)[0]
