from django.db import models

from ads.models import Ads
from campaigns.models import Campaign


class GoogleCampaign(models.Model):
    campaign = models.ForeignKey(Campaign)
    campaign_google_id = models.CharField(max_length=128, null=True, blank=True)
    last_update_ts = models.DateTimeField(auto_now=True)
    raw_data_result = models.TextField(null=True, blank=True)
    group_id = models.CharField(max_length=128, null=True, blank=True)


class GoogleAds(models.Model):
    ads = models.ForeignKey(Ads)
    ads_google_id = models.CharField(max_length=128, null=True, blank=True)
    last_update_ts = models.DateTimeField(auto_now=True)
    raw_data_result = models.TextField(null=True, blank=True)
