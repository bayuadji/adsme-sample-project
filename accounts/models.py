from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User)

    def is_google_connect(self):
        return True

    def is_facebook_connect(self):
        return True
