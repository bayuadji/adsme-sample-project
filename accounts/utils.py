def get_token(user, provider):
    """
    get authentication token, based on provider
    """
    social_auth = user.social_auth.filter(provider=provider)
    if social_auth.count() == 0:
        raise Exception("User hasn't been connected to %s" % provider)

    return social_auth[0].tokens['access_token']


def get_id(user, provider):
    social_auth = user.social_auth.filter(provider=provider)
    if social_auth.count() == 0:
        raise Exception("User hasn't been connected to %s" % provider)

    return social_auth[0].id
