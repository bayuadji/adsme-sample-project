from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.views import login

from django.http import HttpResponseRedirect


def custom_login(request, **kwargs):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    return login(request, **kwargs)


class AccountConnectView(TemplateView):
    template_name = 'account/connect.html'

    def dispatch(self, *args, **kwargs):
        return super(AccountConnectView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountConnectView, self).get_context_data(**kwargs)
        context['is_google'] = self.request.user.social_auth.filter(provider='google-oauth2').count() > 0
        context['is_facebook'] = self.request.user.social_auth.filter(provider='facebook').count() > 0
        return context
