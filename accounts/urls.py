from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import AccountConnectView

urlpatterns = patterns('',
    url(r'connect/$', AccountConnectView.as_view(), name='account.connect')
)
